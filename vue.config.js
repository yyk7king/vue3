const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  productionSourceMap: false,
  devServer: {
    port: 5004
  },
  transpileDependencies: true,
  css: {
    sourceMap: true,
    loaderOptions: {
      postcss: {
        postcssOptions: {
          plugins: [
            require('postcss-pxtorem')({
              rootValue: '10',
              propList: ['*'],
              unitPrecision: 2,
              mediaQueries: false
            })
          ]
        }
      }
    }
  }

})
