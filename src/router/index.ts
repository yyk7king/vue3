import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'layout',
    component: () => import('@/components/layout/layout.vue'),
    meta: {
      layout: 'base'
    }
  },
  {
    path: '/svg/circle',
    name: 'circle',
    component: () => import('@/components/svg/circle.vue'),
    meta: {
      layout: ''
    }
  },
  {
    path: '/common/swiper',
    name: 'swiper',
    component: () => import('@/components/common/swiper.vue'),
    meta: {
      layout: ''
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
